package no.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class takes care of checking a given hand of cards for for example flush, su, of faces ect..
 */
public class CheckHand {

    private ArrayList<PlayingCard> hand;

    /**
     * @param hand - List containing the cards on hand
     */
    public CheckHand(ArrayList<PlayingCard> hand){
        this.hand = hand;
    }

    /**
     * Checks of the hand is flush, i checks if all the cards on hand have the same suit
     * @return - true/false representing if hand is flush
     */
    public boolean checkFlush(){
        char suit = hand.get(0).getSuit();
        List<PlayingCard> flushCheck = hand.stream().filter(p -> p.getSuit()==suit).collect(Collectors.toList());
        return flushCheck.size() == hand.size();
    }

    /**
     * Sums up the total sum of the faces of the cards
     * here, the ess has the value of 1
     * @return - integer representing the sum
     */
    public int sumOfFaces(){
        int sum = hand.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();
        return sum;
    }

    /**
     * Method looks for cards of hearts in the hand and returns a list of all the cards of hearts
     * @return List-object representing all the cards of hearts
     */
    public List<PlayingCard> cardsOfHearts(){
        return hand.stream().filter(p -> p.getSuit()=='H').collect(Collectors.toList());
    }

    /**
     * Checks if the hand contains the the Queen of spades card
     * @return - true/false to confirm/refute
     */
    public boolean queenOfSpades(){
        return hand.stream().anyMatch(p -> p.getAsString().equals("S12"));
    }

}
