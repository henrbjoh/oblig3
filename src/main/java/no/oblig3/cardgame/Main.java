package no.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    Stage window;

    ArrayList<PlayingCard> hand;

    @Override
    public void start(Stage primaryStage) {
        DeckOfCards deck = new DeckOfCards();

        window = primaryStage;
        window.setTitle("CardGame");

        GridPane grid = new GridPane();
        grid.setPadding(new Insets(10,10,10,10));
        grid.setVgap(10);
        grid.setHgap(10);

        Label handLable = new Label("How many cards on hand?");
        GridPane.setConstraints(handLable,0,0);

        TextField handInput = new TextField("5");
        GridPane.setConstraints(handInput,1,0);

        Button buttonDealHand = new Button("Deal hand");
        GridPane.setConstraints(buttonDealHand,0,1);

        Label handDisplay = new Label("");
        handDisplay.setWrapText(true);
        handDisplay.setMaxWidth(140);
        GridPane.setConstraints(handDisplay,0,2);

        buttonDealHand.setOnAction(e -> {
            int numberOfCardsOnHand = Integer.parseInt(handInput.getText());
            if (numberOfCardsOnHand<5){
                handDisplay.setText("Can not deal a hand of less than 5 cards");
            }
            else if (deck.getDeck().size()<numberOfCardsOnHand){
                handDisplay.setText("Not enough cards left in deck to deal hand");

            }
            else {
                this.hand = deck.dealHand(numberOfCardsOnHand);
                String handString = "";
                for (PlayingCard p : hand) {
                    handString += p.getAsString() + "; ";
                }
                handDisplay.setText("The cards on hand are: " + handString);
            }
        });

        Button buttonCheckHand = new Button("Check hand");
        GridPane.setConstraints(buttonCheckHand,0,3);

        Label handFlush = new Label("");
        GridPane.setConstraints(handFlush,0,4);

        Label handSum = new Label("");
        GridPane.setConstraints(handSum,1,4);

        Label handCardsOfHearts = new Label("");
        GridPane.setConstraints(handCardsOfHearts,0,5);
        handCardsOfHearts.setWrapText(true);
        handCardsOfHearts.setMaxWidth(150);

        Label handQueenOfSpades = new Label("");
        GridPane.setConstraints(handQueenOfSpades,1,5);


        buttonCheckHand.setOnAction(e -> {
            CheckHand checkHand = new CheckHand(hand);
            boolean flush = checkHand.checkFlush();
            if (flush){
                handFlush.setText("Flush: Yes");
            }
            else {
                handFlush.setText("Flush: No");
            }

            handSum.setText("Sum of faces: " + checkHand.sumOfFaces());
            List<PlayingCard> cardsOfHearts = checkHand.cardsOfHearts();
            String cardOfHeartsString = "";
            for (PlayingCard p : cardsOfHearts){
                cardOfHeartsString += p.getAsString() + "; ";
            }
            if (cardOfHeartsString.equals("")){
                handCardsOfHearts.setText("No cards of hearts");
            }
            else {
                handCardsOfHearts.setText("Cards of hearts: " + cardOfHeartsString);
            }
            if (checkHand.queenOfSpades()){
                handQueenOfSpades.setText("Queen of spades: Yes");
            }
            else {
                handQueenOfSpades.setText("Queen of spades: No");
            }

        });

        grid.getChildren().addAll(handLable,handInput,buttonDealHand,handDisplay,buttonCheckHand,handFlush,handSum,handCardsOfHearts,handQueenOfSpades);
        Scene scene = new Scene(grid,400,350);
        window.setScene(scene);
        window.show();
    }
    public static void main(String[] args) {
        launch(args);
    }

}
