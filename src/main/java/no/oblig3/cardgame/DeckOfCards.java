package no.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class represents a full deck of cards with 52 cards
 */
public class DeckOfCards {
    private final char[] suit = { 'S', 'H', 'D', 'C' };

    private ArrayList<PlayingCard> deck;

    /**
     * Constructor makes the new, full deck
     */
    public DeckOfCards(){
        deck = new ArrayList<>();

        for (char c : suit){
            for (int i = 1; i<14; i++){
                deck.add(new PlayingCard(c,i));
            }
        }
    }

    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Takes a integer and pulls this amount of random cards from the deck
     * @param n - the amount of cards to be dealt
     * @return - ArrayList containing the cards
     */
    public ArrayList<PlayingCard> dealHand(int n){
        ArrayList<PlayingCard> cardsToHand = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < n; i++) {
            int randomNumber = random.nextInt(deck.size()); //Random number representing a card in the deck
            cardsToHand.add(deck.get(randomNumber));
            deck.remove(deck.get(randomNumber));
        }
        return cardsToHand;
    }

}

