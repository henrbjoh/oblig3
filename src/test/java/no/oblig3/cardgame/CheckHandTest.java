package no.oblig3.cardgame;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CheckHandTest {
    /**
     * Tests the checkFlush-method in a scenario where the outcome is supposed to be true
     */
    @Test
    void testCheckFlushTrue() {
        //Check with a hand that is flush
        ArrayList<PlayingCard> hand = new ArrayList<>();
        hand.add(new PlayingCard('S', 12));
        hand.add(new PlayingCard('S', 3));
        CheckHand checkHandTrue = new CheckHand(hand);
        boolean flushTrue = checkHandTrue.checkFlush();
        assertTrue(true);
    }
    /**
     * Tests the checkFlush-method in a scenario where the outcome is supposed to be false
     */
    @Test
    void testCheckFlushFalse(){
        //Check with a hand that is not flush
        ArrayList<PlayingCard> hand2 = new ArrayList<>();
        hand2.add(new PlayingCard('H',10));
        hand2.add(new PlayingCard('S',4));
        CheckHand checkHandFalse = new CheckHand(hand2);
        boolean flushFalse = checkHandFalse.checkFlush();
        assertFalse(false);
    }

    /**
     * Checks if the returned sum of faces is correct
     */
    @Test
    void testSumOfFaces(){
        ArrayList<PlayingCard> hand = new ArrayList<>();
        hand.add(new PlayingCard('S',12));
        hand.add(new PlayingCard('H',3));
        hand.add(new PlayingCard('H',5));
        CheckHand checkHand = new CheckHand(hand);
        int sum = checkHand.sumOfFaces();
        assertEquals(20,sum);
    }

    /**
     * Checks that the cardOfHearts-method returned the correct card
     */
    @Test
    void testCardsOfHearts(){
        ArrayList<PlayingCard> hand = new ArrayList<>();
        hand.add(new PlayingCard('S',12));
        hand.add(new PlayingCard('H',5));
        CheckHand checkHand = new CheckHand(hand);
        List<PlayingCard> cardsOfHearts = checkHand.cardsOfHearts();
        assertEquals(hand.get(1),cardsOfHearts.get(0));
    }

    /**
     * tests if the queenOfSpades-method returns the correct boolean
     * In this case it is supposed to return true
     */
    @Test
    void testQueenOfSpadesTrue(){
        ArrayList<PlayingCard> hand = new ArrayList<>();
        hand.add(new PlayingCard('S',12));
        hand.add(new PlayingCard('H',3));
        hand.add(new PlayingCard('H',5));
        CheckHand checkHand = new CheckHand(hand);
        boolean cardFound = checkHand.queenOfSpades();
        assertTrue(cardFound);
    }
    /**
     * tests if the queenOfSpades-method returns the correct boolean
     * In this case it is supposed to return true
     */
    @Test
    void testQueenOfSpadesFalse(){
        ArrayList<PlayingCard> hand = new ArrayList<>();
        hand.add(new PlayingCard('H',12));
        hand.add(new PlayingCard('H',5));
        CheckHand checkHand = new CheckHand(hand);
        boolean cardFound = checkHand.queenOfSpades();
        assertFalse(cardFound);
    }
}